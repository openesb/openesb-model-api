package net.openesb.model.api;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@XmlRootElement
public class ServiceUnit extends Component {

    private String target;
    private String deployedOn;
    private String serviceAssembly;
    private State state = State.UNKNOWN;

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getDeployedOn() {
        return deployedOn;
    }

    public void setDeployedOn(String deployedOn) {
        this.deployedOn = deployedOn;
    }

    public String getServiceAssembly() {
        return serviceAssembly;
    }

    public void setServiceAssembly(String serviceAssembly) {
        this.serviceAssembly = serviceAssembly;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "Name = "
                + this.getName()
                + "\nDescription = "
                + this.getDescription()
                + "\nState = "
                + this.getState()
                + "\nDeployed On = "
                + this.getDeployedOn();
    }
}
