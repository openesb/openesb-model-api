package net.openesb.model.api;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@XmlRootElement
public class BindingComponent extends JBIComponent {

    @Override
    public ComponentType getType() {
        return ComponentType.BINDING_COMPONENT;
    }
}
