package net.openesb.model.api.metric;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface Gauge<T> extends Metric {
    
    T getValue();
}
