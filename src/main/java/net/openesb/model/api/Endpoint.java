package net.openesb.model.api;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@XmlRootElement
public class Endpoint {
    
    private String name;
    
    private boolean consume;
    
    public Endpoint() {
        // No-arg constructor
    }
    
    public Endpoint(String name, boolean consume) {
        this.name = name;
        this.consume = consume;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isConsume() {
        return consume;
    }

    public void setConsume(boolean consume) {
        this.consume = consume;
    }
}
