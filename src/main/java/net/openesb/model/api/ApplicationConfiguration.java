package net.openesb.model.api;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ApplicationConfiguration {
    
    private String name;
    private Set<ComponentConfiguration> configurations = new HashSet<ComponentConfiguration>();

    public ApplicationConfiguration(String name) {
        this.name = name;
    }
    
    public ApplicationConfiguration() {
        // No-args constructor
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<ComponentConfiguration> getConfigurations() {
        return configurations;
    }

    public void setConfigurations(Set<ComponentConfiguration> configurations) {
        this.configurations = configurations;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApplicationConfiguration other = (ApplicationConfiguration) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        return true;
    }
}
