package net.openesb.model.api;

import java.util.HashSet;
import java.util.Set;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@XmlRootElement(name = "library")
public class SharedLibrary extends Component {

    private String version;
    private String buildNumber;
    private Set<String> componentDependencies;
            
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
    }

    public Set<String> getComponentDependencies() {
        return componentDependencies;
    }

    public void setComponentDependencies(Set<String> componentDependencies) {
        this.componentDependencies = componentDependencies;
    }
}
