package net.openesb.model.api;

/**
 * A name / value pair parameter.
 * <pre>
 * parameter               = attribute "=" value
 * attribute               = token
 * value                   = token | quoted-string
 * </pre>
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface NameValuePair<T> {

    String getName();

    T getValue();
}
