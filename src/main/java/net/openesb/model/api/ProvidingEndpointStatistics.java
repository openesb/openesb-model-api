package net.openesb.model.api;

import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@XmlRootElement
public class ProvidingEndpointStatistics extends EndpointStatistics {

    /**
     * Activation Time
     */
    private Date activationTime;
    
    /**
     * Up time
     */
    private long upTime;
    
    /**
     * Number of Received Requests
     */
    private long receivedRequests;
    
    /**
     * Number of Sent Replies
     */
    private long sentReplies;

    public long getReceivedRequests() {
        return receivedRequests;
    }

    public void setReceivedRequests(long receivedRequests) {
        this.receivedRequests = receivedRequests;
    }

    public long getSentReplies() {
        return sentReplies;
    }

    public void setSentReplies(long sentReplies) {
        this.sentReplies = sentReplies;
    }

    public Date getActivationTime() {
        return activationTime;
    }

    public void setActivationTime(Date activationTime) {
        this.activationTime = activationTime;
    }

    public long getUpTime() {
        return upTime;
    }

    public void setUpTime(long upTime) {
        this.upTime = upTime;
    }
}
