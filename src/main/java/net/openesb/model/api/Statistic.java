package net.openesb.model.api;

import net.openesb.model.api.metric.Gauge;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class Statistic<T> implements Gauge<T> {
    
    private String name;
    private T value;
    private String description;
    
    public Statistic() {
        
    }
    
    public Statistic(String name, T value, String description) {
        this.name = name;
        this.value = value;
        this.description = description;
    }
    
    public String getDescription() {
        return this.description;
    }

    public String getName() {
        return name;
    }

    @Override
    public T getValue() {
        return value;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
