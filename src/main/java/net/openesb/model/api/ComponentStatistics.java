package net.openesb.model.api;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@XmlRootElement
public class ComponentStatistics {

    /**
     * Number of active MessageExchanges
     */
    private long activeExchanges;
    
    /**
     * Number of active MessageExchanges Max
     */
    private long maxActiveExchanges;
    
    /**
     * Number of queued MessageExchanges
     */
    private long queuedExchanges;
    
    /**
     * Number of queued MessageExchanges Max
     */
    private long maxQueuedExchanges;
    
    /**
     * Number of active Endpoints
     */
    private long activeEndpoints;
    
    /**
     * Number of requests sent
     */
    private long sendRequest;
    
    /**
     * Number of requests received
     */
    private long receiveRequest;
    
    /**
     * Number of replies sent
     */
    private long sendReply;
    
    /**
     * Number of replies received
     */
    private long receiveReply;
    
    /**
     * Number of faults sent
     */
    private long sendFault;
    
    /**
     * Number of faults received
     */
    private long receiveFault;
    
    /**
     * Number of DONE requests sent
     */
    private long sendDone;
    
    /**
     * Number of DONE requests received
     */
    private long receiveDone;
    
    /**
     * Number of ERROR requests sent
     */
    private long sendError;
    
    /**
     * Number of ERROR requests received
     */
    private long receiveError;
    
    /**
     * Number of waits that didn't find any work
     */
    private long deadWait;
    
    /**
     * Number of requests that timed out
     */
    private long timeOut;
    
    /**
     * Number of Send operations
     */
    private long sendCount;
    
    /**
     * Number of SendSync operations
     */
    private long sendSyncCount;
    
    /**
     * Number of Accept operations
     */
    private long acceptCount;
    
    /**
     * Response Time Min
     */
    private long responseTimeMin;
    
    /**
     * Response Time Avg
     */
    private long responseTimeAvg;
    
    /**
     * Response Time Max
     */
    private long responseTimeMax;
    
    /**
     * Response Time Std
     */
    private long responseTimeStd;
    
    /**
     * Status Time Min
     */
    private long statusTimeMin;
    
    /**
     * Status Time Avg
     */
    private long statusTimeAvg;
    
    /**
     * Status Time Max
     */
    private long statusTimeMax;
    
    /**
     * Status Time Std
     */
    private long statusTimeStd;
    
    /**
     * NMR Time Min
     */
    private long nmrTimeMin;
    
    /**
     * NMR Time Avg
     */
    private long nmrTimeAvg;
    
    /**
     * NMR Time Max
     */
    private long nmrTimeMax;
    
    /**
     * NMR Time Std
     */
    private long nmrTimeStd;
    
    /**
     * Component Time Min
     */
    private long componentTimeMin;
    
    /**
     * Component Time Avg
     */
    private long componentTimeAvg;
    
    /**
     * Component Time Max
     */
    private long componentTimeMax;
    
    /**
     * Component Time Std
     */
    private long componentTimeStd;
    
    /**
     * Channel Time Min
     */
    private long channelTimeMin;
    
    /**
     * Channel Time Avg
     */
    private long channelTimeAvg;
    
    /**
     * Channel Time Max
     */
    private long channelTimeMax;
    
    /**
     * Channel Time Std
     */
    private long channelTimeStd;

    public long getActiveExchanges() {
        return activeExchanges;
    }

    public void setActiveExchanges(long activeExchanges) {
        this.activeExchanges = activeExchanges;
    }

    public long getMaxActiveExchanges() {
        return maxActiveExchanges;
    }

    public void setMaxActiveExchanges(long maxActiveExchanges) {
        this.maxActiveExchanges = maxActiveExchanges;
    }

    public long getQueuedExchanges() {
        return queuedExchanges;
    }

    public void setQueuedExchanges(long queuedExchanges) {
        this.queuedExchanges = queuedExchanges;
    }

    public long getMaxQueuedExchanges() {
        return maxQueuedExchanges;
    }

    public void setMaxQueuedExchanges(long maxQueuedExchanges) {
        this.maxQueuedExchanges = maxQueuedExchanges;
    }

    public long getActiveEndpoints() {
        return activeEndpoints;
    }

    public void setActiveEndpoints(long activeEndpoints) {
        this.activeEndpoints = activeEndpoints;
    }

    public long getSendRequest() {
        return sendRequest;
    }

    public void setSendRequest(long sendRequest) {
        this.sendRequest = sendRequest;
    }

    public long getReceiveRequest() {
        return receiveRequest;
    }

    public void setReceiveRequest(long receiveRequest) {
        this.receiveRequest = receiveRequest;
    }

    public long getSendReply() {
        return sendReply;
    }

    public void setSendReply(long sendReply) {
        this.sendReply = sendReply;
    }

    public long getReceiveReply() {
        return receiveReply;
    }

    public void setReceiveReply(long receiveReply) {
        this.receiveReply = receiveReply;
    }

    public long getSendFault() {
        return sendFault;
    }

    public void setSendFault(long sendFault) {
        this.sendFault = sendFault;
    }

    public long getReceiveFault() {
        return receiveFault;
    }

    public void setReceiveFault(long receiveFault) {
        this.receiveFault = receiveFault;
    }

    public long getSendDone() {
        return sendDone;
    }

    public void setSendDone(long sendDone) {
        this.sendDone = sendDone;
    }

    public long getReceiveDone() {
        return receiveDone;
    }

    public void setReceiveDone(long receiveDone) {
        this.receiveDone = receiveDone;
    }

    public long getSendError() {
        return sendError;
    }

    public void setSendError(long sendError) {
        this.sendError = sendError;
    }

    public long getReceiveError() {
        return receiveError;
    }

    public void setReceiveError(long receiveError) {
        this.receiveError = receiveError;
    }

    public long getDeadWait() {
        return deadWait;
    }

    public void setDeadWait(long deadWait) {
        this.deadWait = deadWait;
    }

    public long getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(long timeOut) {
        this.timeOut = timeOut;
    }

    public long getSendCount() {
        return sendCount;
    }

    public void setSendCount(long sendCount) {
        this.sendCount = sendCount;
    }

    public long getSendSyncCount() {
        return sendSyncCount;
    }

    public void setSendSyncCount(long sendSyncCount) {
        this.sendSyncCount = sendSyncCount;
    }

    public long getAcceptCount() {
        return acceptCount;
    }

    public void setAcceptCount(long acceptCount) {
        this.acceptCount = acceptCount;
    }

    public long getResponseTimeMin() {
        return responseTimeMin;
    }

    public void setResponseTimeMin(long responseTimeMin) {
        this.responseTimeMin = responseTimeMin;
    }

    public long getResponseTimeAvg() {
        return responseTimeAvg;
    }

    public void setResponseTimeAvg(long responseTimeAvg) {
        this.responseTimeAvg = responseTimeAvg;
    }

    public long getResponseTimeMax() {
        return responseTimeMax;
    }

    public void setResponseTimeMax(long responseTimeMax) {
        this.responseTimeMax = responseTimeMax;
    }

    public long getResponseTimeStd() {
        return responseTimeStd;
    }

    public void setResponseTimeStd(long responseTimeStd) {
        this.responseTimeStd = responseTimeStd;
    }

    public long getStatusTimeMin() {
        return statusTimeMin;
    }

    public void setStatusTimeMin(long statusTimeMin) {
        this.statusTimeMin = statusTimeMin;
    }

    public long getStatusTimeAvg() {
        return statusTimeAvg;
    }

    public void setStatusTimeAvg(long statusTimeAvg) {
        this.statusTimeAvg = statusTimeAvg;
    }

    public long getStatusTimeMax() {
        return statusTimeMax;
    }

    public void setStatusTimeMax(long statusTimeMax) {
        this.statusTimeMax = statusTimeMax;
    }

    public long getStatusTimeStd() {
        return statusTimeStd;
    }

    public void setStatusTimeStd(long statusTimeStd) {
        this.statusTimeStd = statusTimeStd;
    }

    public long getNmrTimeMin() {
        return nmrTimeMin;
    }

    public void setNmrTimeMin(long nmrTimeMin) {
        this.nmrTimeMin = nmrTimeMin;
    }

    public long getNmrTimeAvg() {
        return nmrTimeAvg;
    }

    public void setNmrTimeAvg(long nmrTimeAvg) {
        this.nmrTimeAvg = nmrTimeAvg;
    }

    public long getNmrTimeMax() {
        return nmrTimeMax;
    }

    public void setNmrTimeMax(long nmrTimeMax) {
        this.nmrTimeMax = nmrTimeMax;
    }

    public long getNmrTimeStd() {
        return nmrTimeStd;
    }

    public void setNmrTimeStd(long nmrTimeStd) {
        this.nmrTimeStd = nmrTimeStd;
    }

    public long getComponentTimeMin() {
        return componentTimeMin;
    }

    public void setComponentTimeMin(long componentTimeMin) {
        this.componentTimeMin = componentTimeMin;
    }

    public long getComponentTimeAvg() {
        return componentTimeAvg;
    }

    public void setComponentTimeAvg(long componentTimeAvg) {
        this.componentTimeAvg = componentTimeAvg;
    }

    public long getComponentTimeMax() {
        return componentTimeMax;
    }

    public void setComponentTimeMax(long componentTimeMax) {
        this.componentTimeMax = componentTimeMax;
    }

    public long getComponentTimeStd() {
        return componentTimeStd;
    }

    public void setComponentTimeStd(long componentTimeStd) {
        this.componentTimeStd = componentTimeStd;
    }

    public long getChannelTimeMin() {
        return channelTimeMin;
    }

    public void setChannelTimeMin(long channelTimeMin) {
        this.channelTimeMin = channelTimeMin;
    }

    public long getChannelTimeAvg() {
        return channelTimeAvg;
    }

    public void setChannelTimeAvg(long channelTimeAvg) {
        this.channelTimeAvg = channelTimeAvg;
    }

    public long getChannelTimeMax() {
        return channelTimeMax;
    }

    public void setChannelTimeMax(long channelTimeMax) {
        this.channelTimeMax = channelTimeMax;
    }

    public long getChannelTimeStd() {
        return channelTimeStd;
    }

    public void setChannelTimeStd(long channelTimeStd) {
        this.channelTimeStd = channelTimeStd;
    }
}
