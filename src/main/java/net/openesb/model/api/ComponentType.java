package net.openesb.model.api;

import javax.xml.bind.annotation.XmlEnum;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@XmlEnum
public enum ComponentType {
 
    /**
     * type Binding Component.
     */
    BINDING_COMPONENT,
    
    /**
     * type Service Engine.
     */
    SERVICE_ENGINE,
    
    /**
     * type Shared Libraries.
     */
    SHARED_LIBRARY;

    @Override
    public String toString() {
        return this.name().toLowerCase();
    }
}
