package net.openesb.model.api;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@XmlRootElement
public class ConsumingEndpointStatistics extends EndpointStatistics {

    /**
     * Number of Sent Requests
     */
    private long sentRequests;
    
    /**
     * Number of Received Replies
     */
    private long receivedReplies;

    public long getSentRequests() {
        return sentRequests;
    }

    public void setSentRequests(long sentRequests) {
        this.sentRequests = sentRequests;
    }

    public long getReceivedReplies() {
        return receivedReplies;
    }

    public void setReceivedReplies(long receivedReplies) {
        this.receivedReplies = receivedReplies;
    }
}
