package net.openesb.model.api;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@XmlRootElement
public class ComponentDescriptor {
    
    private String name;
    
    private String description;
    
    private String version;
    
    private String buildNumber;
    
    private List<String> classPathElements;
    
    private String componentClassName;
    
    private ComponentType type;
    
    private String installRoot;
    
    private String workspaceRoot;
    
    private List<String> sharedLibraries;
    
    private boolean bootstrapClassLoaderSelfFirst;
    
    private boolean classLoaderSelfFirst;
    
    private List<String> bootstrapClassPathElements;
    
    private String bootstrapClassName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
    }

    public List<String> getClassPathElements() {
        return classPathElements;
    }

    public void setClassPathElements(List<String> classPathElements) {
        this.classPathElements = classPathElements;
    }

    public String getComponentClassName() {
        return componentClassName;
    }

    public void setComponentClassName(String componentClassName) {
        this.componentClassName = componentClassName;
    }

    public ComponentType getType() {
        return type;
    }

    public void setType(ComponentType type) {
        this.type = type;
    }

    public String getInstallRoot() {
        return installRoot;
    }

    public void setInstallRoot(String installRoot) {
        this.installRoot = installRoot;
    }

    public String getWorkspaceRoot() {
        return workspaceRoot;
    }

    public void setWorkspaceRoot(String workspaceRoot) {
        this.workspaceRoot = workspaceRoot;
    }

    public List<String> getSharedLibraries() {
        return sharedLibraries;
    }

    public void setSharedLibraries(List<String> sharedLibraries) {
        this.sharedLibraries = sharedLibraries;
    }

    public boolean isBootstrapClassLoaderSelfFirst() {
        return bootstrapClassLoaderSelfFirst;
    }

    public void setBootstrapClassLoaderSelfFirst(boolean bootstrapClassLoaderSelfFirst) {
        this.bootstrapClassLoaderSelfFirst = bootstrapClassLoaderSelfFirst;
    }

    public boolean isClassLoaderSelfFirst() {
        return classLoaderSelfFirst;
    }

    public void setClassLoaderSelfFirst(boolean classLoaderSelfFirst) {
        this.classLoaderSelfFirst = classLoaderSelfFirst;
    }

    public List<String> getBootstrapClassPathElements() {
        return bootstrapClassPathElements;
    }

    public void setBootstrapClassPathElements(List<String> bootstrapClassPathElements) {
        this.bootstrapClassPathElements = bootstrapClassPathElements;
    }

    public String getBootstrapClassName() {
        return bootstrapClassName;
    }

    public void setBootstrapClassName(String bootstrapClassName) {
        this.bootstrapClassName = bootstrapClassName;
    }
}
