package net.openesb.model.api;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@XmlRootElement
public class NMR {
    
    private List<String> channels;
    
    private int endpointCount;

    public List<String> getChannels() {
        return channels;
    }

    public void setChannels(List<String> channels) {
        this.channels = channels;
    }

    public int getEndpointCount() {
        return endpointCount;
    }

    public void setEndpointCount(int endpointCount) {
        this.endpointCount = endpointCount;
    }
}
