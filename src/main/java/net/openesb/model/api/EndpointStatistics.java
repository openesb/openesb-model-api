package net.openesb.model.api;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public abstract class EndpointStatistics {

    /**
     * Name of the owning component
     */
    private String owningComponent;
    /**
     * Number of active exchanges
     */
    private long activeExchanges;
    /**
     * Number of Received DONEs
     */
    private long receivedDones;
    /**
     * Number of Sent DONEs
     */
    private long sentDones;
    /**
     * Number of Received Faults
     */
    private long receivedFaults;
    /**
     * Number of Sent Faults
     */
    private long sentFaults;
    /**
     * Number of Received Errors
     */
    private long receivedErrors;
    /**
     * Number of Sent Errors
     */
    private long sentErrors;

    public String getOwningComponent() {
        return owningComponent;
    }

    public void setOwningComponent(String owningComponent) {
        this.owningComponent = owningComponent;
    }

    public long getActiveExchanges() {
        return activeExchanges;
    }

    public void setActiveExchanges(long activeExchanges) {
        this.activeExchanges = activeExchanges;
    }

    public long getReceivedDones() {
        return receivedDones;
    }

    public void setReceivedDones(long receivedDones) {
        this.receivedDones = receivedDones;
    }

    public long getSentDones() {
        return sentDones;
    }

    public void setSentDones(long sentDones) {
        this.sentDones = sentDones;
    }

    public long getReceivedFaults() {
        return receivedFaults;
    }

    public void setReceivedFaults(long receivedFaults) {
        this.receivedFaults = receivedFaults;
    }

    public long getSentFaults() {
        return sentFaults;
    }

    public void setSentFaults(long sentFaults) {
        this.sentFaults = sentFaults;
    }

    public long getReceivedErrors() {
        return receivedErrors;
    }

    public void setReceivedErrors(long receivedErrors) {
        this.receivedErrors = receivedErrors;
    }

    public long getSentErrors() {
        return sentErrors;
    }

    public void setSentErrors(long sentErrors) {
        this.sentErrors = sentErrors;
    }
}
