package net.openesb.model.api;

import javax.xml.bind.annotation.XmlEnum;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@XmlEnum
public enum State {

    /**
     * status Unknown.
     */
    UNKNOWN("Unknown"),
    
    /**
     * status Deployed.
     */
    STARTED("Started"),
    
    /**
     * status Deployed.
     */
    STOPPED("Stopped"),
    
    /**
     * status Deployed.
     */
    SHUTDOWN("Shutdown");
    
    private String state;

    private State(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    @Override
    public String toString() {
        return this.getState().toLowerCase();
    }
    
    public static State from(String state) {
        for(State st : State.values()) {
            if (st.getState().equalsIgnoreCase(state)) {
                return st;
            }
        }
        
        return UNKNOWN;
    }
}
