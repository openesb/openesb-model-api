package net.openesb.model.api;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@XmlRootElement
public class Logger implements NameValuePair<String> {
    
    private String name;
    private String displayName;
    private String level;
    private String localizedLevel;

    public Logger() {
        
    }
    
    public Logger(String name, String level) {
        this.name = name;
        this.level = level;
    }
    
    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getLocalizedLevel() {
        return localizedLevel;
    }

    public void setLocalizedLevel(String localizedLevel) {
        this.localizedLevel = localizedLevel;
    }

    @Override
    @XmlTransient
    public String getValue() {
        return getLevel();
    }
}
