package net.openesb.model.api;

import java.util.Set;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@XmlRootElement(name = "assembly")
public class ServiceAssembly extends Component {

    private State state = State.UNKNOWN;
    private Set<ServiceUnit> serviceUnits;

    public Set<ServiceUnit> getServiceUnits() {
        return serviceUnits;
    }

    public void setServiceUnits(Set<ServiceUnit> serviceUnits) {
        this.serviceUnits = serviceUnits;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "Name = "
                + this.getName()
                + "\nDescription = "
                + this.getDescription()
                + "\nState = "
                + this.getState()
                + "\nService Units = "
                + this.getServiceUnits().size();

    }
}
