package net.openesb.model.api;

import java.util.Set;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@XmlRootElement(name = "component")
public abstract class JBIComponent extends Component {

    private String version;
    private String buildNumber;
    private State state = State.UNKNOWN;
    private Set<ServiceUnit> serviceUnits;

    private boolean supportApplicationVariables = false;
    private boolean supportApplicationConfigurations = false;
    private boolean supportComponentConfigurations = false;
    
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
    }

    public Set<ServiceUnit> getServiceUnits() {
        return serviceUnits;
    }

    public void setServiceUnits(Set<ServiceUnit> serviceUnits) {
        this.serviceUnits = serviceUnits;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public boolean isSupportApplicationVariables() {
        return supportApplicationVariables;
    }

    public void setSupportApplicationVariables(boolean supportApplicationVariables) {
        this.supportApplicationVariables = supportApplicationVariables;
    }

    public boolean isSupportApplicationConfigurations() {
        return supportApplicationConfigurations;
    }

    public void setSupportApplicationConfigurations(boolean supportApplicationConfigurations) {
        this.supportApplicationConfigurations = supportApplicationConfigurations;
    }

    public boolean isSupportComponentConfigurations() {
        return supportComponentConfigurations;
    }

    public void setSupportComponentConfigurations(boolean supportComponentConfigurations) {
        this.supportComponentConfigurations = supportComponentConfigurations;
    }

    public abstract ComponentType getType();

    @Override
    public String toString() {
        return "\nType = "
                + this.getType()
                + "\nName = "
                + this.getName()
                + "\nDescription = "
                + this.getDescription()
                + "\nState = "
                + this.getState()
                + "\nComponent Version = "
                + this.getVersion()
                + "\nBuild Number = "
                + this.getBuildNumber();
    }
}
