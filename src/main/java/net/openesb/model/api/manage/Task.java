package net.openesb.model.api.manage;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class Task {
    
    private String component;
    
    private FrameworkTask frameworkTask;
    private Set<ComponentTask> componentsTask = new HashSet<ComponentTask>();

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public FrameworkTask getFrameworkTask() {
        return frameworkTask;
    }

    public void setFrameworkTask(FrameworkTask frameworkTask) {
        this.frameworkTask = frameworkTask;
    }

    public Set<ComponentTask> getComponentsTask() {
        return componentsTask;
    }

    public void setComponentsTask(Set<ComponentTask> componentsTask) {
        this.componentsTask = componentsTask;
    }
}
