package net.openesb.model.api.manage;

import java.util.List;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class TaskResult {
    
    public enum TaskResultType  {
        SUCCESS, 
        FAILED;
    
        private TaskResultType() {
        }
        
        public static TaskResultType convert(String input) {
            for(TaskResultType type : values()) {
                if (type.name().equalsIgnoreCase(input)) {
                    return type;
                }
            }
            
            return null;
        }
    };
    
    public enum MessageType {
        ERROR,
        WARNING,
        INFO;
    
        private MessageType() {
        }
        
        public static MessageType convert(String input) {
            for(MessageType type : values()) {
                if (type.name().equalsIgnoreCase(input)) {
                    return type;
                }
            }
            
            return null;
        }
    };
    
    private String taskId;
    private TaskResultType taskResult = TaskResultType.SUCCESS;
    private MessageType messageType;
    private List<String> taskStatusMsg;
    private List<String> errorMsg;
    
    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public TaskResultType getTaskResult() {
        return taskResult;
    }

    public void setTaskResult(TaskResultType taskResult) {
        this.taskResult = taskResult;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public List<String> getTaskStatusMsg() {
        return taskStatusMsg;
    }

    public void setTaskStatusMsg(List<String> taskStatusMsg) {
        this.taskStatusMsg = taskStatusMsg;
    }

    public List<String> getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(List<String> errorMsg) {
        this.errorMsg = errorMsg;
    }
}
