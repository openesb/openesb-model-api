package net.openesb.model.api;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@XmlRootElement
public class ServiceEngine extends JBIComponent {
    
    @Override
    public ComponentType getType() {
        return ComponentType.SERVICE_ENGINE;
    }
}
